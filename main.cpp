#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

int main(){
    srand (time(NULL));
    int guess = rand() % 1000 + 1;
    int Max = 1000, Min = 1, attempts = 0;
    string svar;
    cout << "I think your number is: " << guess << endl;
    for(attempts = 1; attempts < 9; attempts++){
        cin >> svar;
        if(svar == "smaller"){
            Max = guess;
            guess = rand() % (Max-Min) + (Min+1);

            cout << "I think your number is: " << guess << endl;
        }
        else if(svar == "bigger"){
            Min = guess;
            guess = rand() % (Max-Min) + (Min+1);

            cout << "I think your number is: " << guess << endl;
        }
        else if(svar == "korrekt"){
            cout << "Det korrekte svar er: " << guess << endl;
            break;
            return 0;
        }
        else{
            cout << "Please type a real answer" << endl;
        }
        }
}
